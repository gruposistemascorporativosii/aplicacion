<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Ejercicio 2 - JSP</title>
</head>
<body>
 <h2>Promedio de 3 notas en JSP</h2>
 <form action="promedio.jsp" method="POST">
     <table>
         <tr>
             <td>Ingrese numero 1 : </td>
             <td><input type="text" name="numero1"></td>
         </tr>
          <tr>
             <td>Ingrese numero 2 : </td>
             <td><input type="text" name="numero2"></td>
         </tr>
         <tr>
             <td>Ingrese numero 3 : </td>
             <td><input type="text" name="numero3"></td>
         </tr>
         <tr>
             <td>
                 <input type="submit" name ="enviar"value="Calcular">
             </td>
         </tr>
     </table>
 </form>
</body>
</html>
<%
if(request.getParameter("enviar") != null)
{
    int num1 = Integer.parseInt(request.getParameter("numero1"));
    int num2 = Integer.parseInt(request.getParameter("numero2"));
    int num3 = Integer.parseInt(request.getParameter("numero3"));
    int promedio = (num1+num2+num3)/3; 
    out.print("El promedio es : " + promedio );
}
%>